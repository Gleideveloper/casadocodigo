package br.com.buritechcourse.agendaescolar.integracao;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by gleidesilva on 18/09/15.
 * Classe usada para acesso ao webService
 */
public class WebClient {
    private final String urlWebService;

    public WebClient(String urlWebService) {
        this.urlWebService = urlWebService;
    }

    private String getStringFromInputStream (InputStream is) {
        BufferedReader reader = null;
        StringBuilder texto = new StringBuilder();
        String line;
        try {
            reader = new BufferedReader (new InputStreamReader(is));
            while ((line = reader.readLine()) != null) {
                texto.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return texto.toString();
    }

    /**
     * Metodo para envio do JSON
     * @param json
     * @return
     */
    public String post (String json) {
        //Definições de comunicação
        HttpURLConnection httpURLConnection = null;

        try {
            URL url = new URL(this.urlWebService);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            //Coloca a String JSON no conteúdo a ser enviado
            httpURLConnection.setRequestMethod("POST");
            /**
             * Configura o tipo de requisição
             */
            //Informa que o conteudo da requisição é JSON e
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            //Solicita que a resposta também seja em JSON
            httpURLConnection.setRequestProperty("Accept", "application/json; charset=utf-8");

            //Coloca a String JSON no conteudo a ser enviado
            OutputStream outputStream = httpURLConnection.getOutputStream();
            //Envio do JSON para o server
            outputStream.write(json.getBytes());
            //Análise da resposta do servidor web
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream is = httpURLConnection.getInputStream();
                return getStringFromInputStream(is);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}