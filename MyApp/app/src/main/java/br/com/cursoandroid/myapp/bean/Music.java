package br.com.cursoandroid.myapp.bean;

/**
 * Created by gleides.s on 22/12/15.
 */
public class Music {
    private String mMusicName = null;
    private String mMusicAlbum = null;
    private int mMusicAlbumIcon = 0;
    private int mMusicFileId = 0;

    public Music(String musicName, String musicAlbum, int musicAlbumIcon, int mMusicFile) {

        mMusicName = musicName;
        mMusicAlbum = musicAlbum;
        mMusicAlbumIcon = musicAlbumIcon;
        mMusicFile = mMusicFile;
    }

    public String getMusicName() {
        return mMusicName;
    }

    public String getMusicAlbumName() {
        return mMusicAlbum;
    }

    public int getMusicAlbumIcon() {
        return mMusicAlbumIcon;
    }

    public int getMusicFileId() {
        return mMusicFileId;
    }
}
