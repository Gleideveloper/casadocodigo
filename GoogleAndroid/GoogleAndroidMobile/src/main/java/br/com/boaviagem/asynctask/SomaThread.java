package br.com.boaviagem.asynctask;

import android.widget.TextView;
import br.com.boaviagem.R;

/**
 * Created by gleides on 26/05/15.
 */
public class SomaThread extends ExemploBaseSoma {
    @Override
    protected void somar(final int vlr1, final int vlr2) {
        new Thread(){
            public void run(){
                int soma = vlr1 + vlr2;
                TextView textView = (TextView) findViewById(R.id.totalSoma);
                textView.setText("Total somar: " + soma);
            }
        }.start();
    }
}
