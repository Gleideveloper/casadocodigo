package br.com.buritechcourse.agendaescolar.helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import br.com.buritechcourse.agendaescolar.activity.ProfessorForm;
import br.com.buritechcourse.agendaescolar.R;
import br.com.buritechcourse.agendaescolar.model.bean.Professor;

/**
 * Created by gleidesilva on 07/07/15.
 */
public class ProfessorHelper {
    //Atributos da view
    private EditText id;
    private EditText nome;
    private EditText telefone;
    private EditText endereco;
    private EditText site;
    private EditText email;
    private ImageView foto;
    //Atributo carregado com os dados da tela
    private Professor professor;

    public ProfessorHelper(ProfessorForm formProfessor) {
        this.nome = (EditText) formProfessor.findViewById(R.id.edNome);
        this.telefone = (EditText) formProfessor.findViewById(R.id.edFone);
        this.endereco= (EditText) formProfessor.findViewById(R.id.edEndereco);
        this.site = (EditText) formProfessor.findViewById(R.id.edSite);
        this.email = (EditText) formProfessor.findViewById(R.id.edEmail);
        this.foto = (ImageView) formProfessor.findViewById(R.id.foto);
        this.professor = new Professor();
    }

    public Professor getProfessor() {
        //Faz a transição dos dados do formulário para o Objeto Professor
        professor.setNome(this.nome.getText().toString());
        professor.setTelefone(this.telefone.getText().toString());
        professor.setEndereco(this.endereco.getText().toString());
        professor.setSite(this.site.getText().toString());
        professor.setEmail(this.email.getText().toString());
        //professor.setFoto(this.foto.getDrawable().toString());
        return this.professor;
    }

    public void setProfessor(Professor professor) {
        //Limpa os dados da Activity ProfessorForm
        nome.setText(professor.getNome());
        telefone.setText(professor.getTelefone());
        endereco.setText(professor.getEndereco());
        site.setText(professor.getSite());
        email.setText(professor.getEmail());
        this.professor = professor;
        //Chamada ao método que atualiza a imagem
        if (professor.getFoto() != null) {
            carregaFotoExternalStorage(professor.getFoto());
        }
    }

    public ImageView getFoto () {
        return foto;
    }
    /**
     * Metodo que carrega um foto,  a partir de um arquivo armazenado no device
     * @param localArquivoFoto -> Local de armazenamento da foto
     */
    public void carregaFotoExternalStorage (String localArquivoFoto){
        Log.d("PROFESSOR_HELPER", "Local Arquivo: " + localArquivoFoto);
        //Carrega arquivo de imagem
        Bitmap imageFoto = BitmapFactory.decodeFile(localArquivoFoto);

        //Gera imagem reduzida de duas formas conforme as instancias abaixo
        //Bitmap imagemFotoReduzida = Bitmap.createScaledBitmap(imageFoto, 100, 100, true);
        Bitmap imagemFotoReduzida = lessResolution(localArquivoFoto, 100, 100);

        //Atualiza foto do professor
        professor.setFoto(localArquivoFoto);
        //Atualizad a imagem exibida na tela do formulário
        foto.setImageBitmap(imagemFotoReduzida);
    }

    public static Bitmap lessResolution (String filePath, int width, int height) {
        int reqHeight = height;
        int reqWidth = width;
        BitmapFactory.Options options = new BitmapFactory.Options();

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);


        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(filePath, options);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }
}
