package br.com.designpatterns.nullobject;

public class Carrinho {

	public double getValor() {
		return 1.0;
	}

	public int getTamanho() {
		return 1;
	}

	public String getNomeUsuario() {
		return "-";
	}

}
