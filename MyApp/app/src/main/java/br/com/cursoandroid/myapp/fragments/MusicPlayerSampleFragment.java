package br.com.cursoandroid.myapp.fragments;

import android.app.Fragment;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;

import java.util.ArrayList;

import br.com.cursoandroid.myapp.R;
import br.com.cursoandroid.myapp.adapters.CustomAdapter;
import br.com.cursoandroid.myapp.bean.Music;

/**
 * Created by gleides.s on 22/12/15.
 */
public class MusicPlayerSampleFragment extends Fragment {
    private static final String TAG = MusicPlayerSampleFragment.class.getSimpleName();
    private ArrayList<Music> music;
    private Button mBtnPrev;
    private Button mBtnPausePlay;
    private Button mBtnNext;
    private ListView mMusciList;
    private SeekBar mSeekbar;
    private MediaPlayer mMediaPlayer;
    private CustomAdapter mCustomAdapter;
    private double finalTime;
    private double startTime;
    private static int onTimeOnly;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        music = new ArrayList<Music>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_layout_player, container, false);
        mBtnPrev = (Button) view.findViewById(R.id.prev_btn);
        mBtnPrev.setOnClickListener(onPreviousClick);
        mBtnPausePlay = (Button) view.findViewById(R.id.pause_play_btn);
        mBtnPausePlay.setOnClickListener(onPausePlayClick);
        mBtnNext = (Button) view.findViewById(R.id.next_btn);
        mBtnNext.setOnClickListener(onNextClick);
        setValues();
        mCustomAdapter = new CustomAdapter(getActivity().getApplicationContext(), music);
        mMusciList = (ListView) view.findViewById(R.id.list_view_player);
        mMusciList.setAdapter(mCustomAdapter);
        mMusciList.setOnItemClickListener(onItemClick);
        mSeekbar = (SeekBar) view.findViewById(R.id.seekbar_player);
        mSeekbar.setClickable(false);
        return view;
    }

    private void setValues() {
        music.add(new Music("Nome","Album", R.mipmap.ic_launcher, R.raw.get_lucky));
        music.add(new Music("Nome","Album", R.mipmap.ic_launcher, R.raw.radioactive));
    }

    AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG,"OnItemClickListener");
            mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.get_lucky);
            mMediaPlayer.start();
            finalTime = mMediaPlayer.getDuration();
            startTime = mMediaPlayer.getCurrentPosition();

            if (onTimeOnly == 0){
                mSeekbar.setMax((int) finalTime);
                onTimeOnly = 1;
            }
            mSeekbar.setProgress((int) startTime);
        }
    };

    View.OnClickListener onPreviousClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    View.OnClickListener onPausePlayClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mMediaPlayer.pause();
        }
    };

    View.OnClickListener onNextClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

}
