package br.com.buritechcourse.agendaescolar.converter;

import android.util.Log;
import br.com.buritechcourse.agendaescolar.model.bean.Conteudo;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by gleidesilva on 22/09/15.
 */
public class ConteudoConverter {
    public Conteudo fromJSON(JSONObject conteudoJSON) {
        Log.d("ConteudoConverter", "JSONObject: " + conteudoJSON);
        Conteudo conteudo = new Conteudo();

        try {
            conteudo.setId(conteudoJSON.getLong("id"));
            conteudo.setDescricao(conteudoJSON.getString("descricao"));
            conteudo.setSala(conteudoJSON.getString("sala"));
            conteudo.setHorario(conteudoJSON.getString("horario"));

            JSONObject dataJSON = conteudoJSON.getJSONObject("data");
            conteudo.setData(dataJSON.getString("year") + "-"
                    + dataJSON.getString("month") + "-"
                    + dataJSON.getString("dayOfMonth"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return conteudo;
    }
}
