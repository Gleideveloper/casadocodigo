package br.com.cursoandroid.myapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import br.com.cursoandroid.myapp.R;
import br.com.cursoandroid.myapp.bean.Music;

/**
 * Created by gleides.s on 22/12/15.
 */
public class CustomAdapter extends ArrayAdapter{

    private WeakReference<Context> mContextWeakReference;
    private int id;
    private List<Music> mMusicItems;
    private TextView mMusicTitle;
    private TextView mMusicAlbum;
    private ImageView mMusicAlbumIcon;


    public CustomAdapter(Context context, ArrayList<Music> musicitems) {
        super(context,0,musicitems);
        mContextWeakReference = new WeakReference<Context>(context);
        mMusicItems = musicitems;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View mView = view;

        if(mView == null){
            mView = LayoutInflater.from(getContext()).inflate(R.layout.content_layout_item_listview, parent,false);
        }
        mMusicTitle = (TextView) mView.findViewById(R.id.music_title_txt);

        if (mMusicItems.get(position) != null){
            Music music = mMusicItems.get(position);
            mMusicTitle.setText(music.getMusicName());
        }
        return mView;
    }
}
