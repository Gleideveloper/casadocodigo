package br.com.buritechcourse.agendaescolar.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import br.com.buritechcourse.agendaescolar.converter.ProfessorConverter;
import br.com.buritechcourse.agendaescolar.integracao.WebClient;
import br.com.buritechcourse.agendaescolar.model.bean.Professor;
import br.com.buritechcourse.agendaescolar.model.dao.ProfessorDAO;

import java.util.List;

/**
 * Created by gleidesilva on 21/09/15.
 * AsyncTask possue três parametros
 * 1. Tipo de parâmetro enviado para a tarefa em execução -> Método doInBackground(Object... params)
 * 2. Tipo de Unidade de Progresso lançada durante o processamento -> Método onProgressUpdate(Object... values)
 * 3. Tipo de retorno da tarefa
 */
public class ProfessorTask extends AsyncTask<Object, Object, String> {
    /**
     * Serviços públicos que podem ser usados para Teste de sincronização
     */
    //Servidor para teste JSON: http://www.jsontest.com/
    private final String url = "http://ip.jsontest.com/";
    //private final String url = "http://192.168.1.129:8080/AlunoWeb/receber-json";

    //Contexto (Tela) para exibição de mensagens
    private Context context;

    //Barra de progresso
    private ProgressDialog progressDialogBar;

    //Construtor que recebe o contexto da App
    public ProfessorTask(Context context) {
        this.context = context;
    }

    /**
     * Método de callBack invocado ANTES da execução da tarefa
     */
    @Override
    protected void onPreExecute() {
        //Executando a barra de progresso
        progressDialogBar = ProgressDialog.show(context, "Aguarde...", "Enviando dados para o servidor web", true, true);
    }

    /**
     * Método de callBack invocado APÓS a conclusão da tarefa
     * @param result
     */
    @Override
    protected void onPostExecute(String result) {
        //Encerra a exibição da barra de progresso
        progressDialogBar.dismiss();
        //Exibindo a resposta do servidor
        Toast.makeText(context, result, Toast.LENGTH_LONG).show();
    }

    /**
     * Método de execução da tarefa - Envio de dados de professores
     * @param params
     * @return String com a resposta do servidor
     */
    @Override
    protected String doInBackground(Object... params) {
        //Lista de professores
        ProfessorDAO dao =  new ProfessorDAO(context);
        List<Professor> lista = dao.listarProfessores();
        dao.close();
        //Conversão da lista para o JSON
        String json = new ProfessorConverter().toJSON(lista);
        //Envio de dados para o servidor remoto
        String jsonResposta = new WebClient(url).post(json);
        //Devolvendo a resposta do servidor
        return jsonResposta;
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        super.onProgressUpdate(values);
    }

}
