package br.com.designpatterns.templatemethod.exemplo1;

public abstract class ClasseAbstrata {
	public void metodoTemplate(){
		passoAlgoritmoA();
		passoAlgoritmoB();
		System.out.println("ClasseAbstrata metodoTemplate");
	}
	protected abstract void passoAlgoritmoA();
	protected abstract void passoAlgoritmoB();
}
