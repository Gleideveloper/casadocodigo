package br.com.buritechcourse.agendaescolar.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import br.com.buritechcourse.agendaescolar.model.bean.Professor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gleidesilva on 08/07/15.
 */
public class ProfessorDAO extends DataBaseHelper {
    //Constantes para controle de versão e padronização
    private static final int VERSAO = 1;
    private static final String TAG_CLASS_NAME = ProfessorDAO.class.getSimpleName();
    private static final int ID_PROFESSOR = 0;
    private static final int NOME_PROFESSOR = 1;
    private static final int FONE_PROFESSOR = 2;
    private static final int ENDERECO_PROFESSOR = 3;
    private static final int SITE_PROFESSOR = 4;
    private static final int EMAIL_PROFESSOR = 5;
    private static final int FOTO_PROFESSOR = 6;

    public ProfessorDAO(Context context){
        //Chamada à classe pai
        super(context);
        Log.i(TAG_CLASS_NAME, " Método construtor");
    }

    public void cadastrarProfessor(Professor professor){
        //Objeto para armazenar os valores dos campos
        ContentValues values = new ContentValues();

        //Definição de valores dos campos da tabela
        values.put("nome", professor.getNome());
        values.put("telefone", professor.getTelefone());
        values.put("endereco", professor.getEndereco());
        values.put("site", professor.getSite());
        values.put("email", professor.getEmail());
        values.put("foto", professor.getFoto());

        //Inserir dados do Professor no banco de dados
        getWritableDatabase().insert(TABELA_PROFESSOR, null, values);
        Log.i(TAG_CLASS_NAME, "Professor Cadastrado: " + professor.getNome());
    }

    public void excluirProfessor (Professor professor){
        //Definicao de array de paramentros
        String[] args =  {professor.getId().toString()};

        //Exclusao do Professor
        getWritableDatabase().delete(TABELA_PROFESSOR, "id=?", args);
        Log.i(TAG_CLASS_NAME, "Professor Excluído: " + professor.getNome());
    }

    public void alterarProfessor (Professor professor) {
        //Objeto para armazenar os valores dos campos
        ContentValues values = new ContentValues();

        //Definição de valores dos campos da tabela
        values.put("nome", professor.getNome());
        values.put("telefone", professor.getTelefone());
        values.put("endereco", professor.getEndereco());
        values.put("site", professor.getSite());
        values.put("email", professor.getEmail());
        values.put("foto", professor.getFoto());

        //Colecao de valores de parametro do SQL
        String[] args = {professor.getId().toString()};

        //Inserir dados do Professor no banco de dados
        getWritableDatabase().update(TABELA_PROFESSOR, values, "id=?", args);
        Log.i(TAG_CLASS_NAME, "Professor Alterado: " + professor.getNome());
    }

    public List<Professor> listarProfessores(){
        //Definição da coleção de professores
        List<Professor> lista = new ArrayList<Professor>();

        //Instrução SQL que será passada para o Curso
        String sql = "Select * from " + TABELA_PROFESSOR + " order by nome";

        //Objeto que recebe os registros do banco de dados
        Cursor cursor = getReadableDatabase().rawQuery(sql,null);
        try {
            //Percorre os registros do curso
            while (cursor.moveToNext()){
                //Criação de nova referencia para Professor
                Professor professor = new Professor();
                //Recupera os dados do professor
                professor.setId(cursor.getLong(ID_PROFESSOR));
                professor.setNome(cursor.getString(NOME_PROFESSOR));
                professor.setTelefone(cursor.getString(FONE_PROFESSOR));
                professor.setEndereco(cursor.getString(ENDERECO_PROFESSOR));
                professor.setSite(cursor.getString(SITE_PROFESSOR));
                professor.setEmail(cursor.getString(EMAIL_PROFESSOR));
                professor.setFoto(cursor.getString(FOTO_PROFESSOR));

                lista.add(professor);
            }
        }catch (SQLException slqException){
            Log.e(TAG_CLASS_NAME, slqException.getMessage());
        }finally {
            cursor.close();
        }
        return lista;
    }
}
