package br.com.boaviagem.asynctask;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import br.com.boaviagem.R;

/**
 * Created by gleides on 26/05/15.
 */
public class SomaComHandler extends ExemploBaseSoma {
    protected static final int SOMAR = 1;
    @Override
    protected void somar(final int vlr1, final int vlr2) {
        new Thread(){
            public void run(){
                int soma = vlr1 + vlr2;
                Bundle bundle = new Bundle();
                bundle.putString("Resultado", String.valueOf(soma));
                Message msg = new Message();
                msg.what = SOMAR;
                /** Podemos passar os valores para mensagem **/
                msg.setData(bundle);
                /** Envia a mensagem para o handler**/
                handler.sendMessage(msg);
            }
        }.start();
    }
    /** Handler utilizado para atualizar a view **/
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            /** O msg.what permite identificar a mensagem **/
            switch(msg.what){
                case SOMAR:
                    /** Recebe a mensagem para atualizar a view **/
                    TextView textView = (TextView) findViewById(R.id.totalSoma);
                    String soma = (String) msg.getData().getString("Resultado");
                    textView.setText(String.valueOf("Soma: " + soma));
                    break;
                default:
                    break;
            }
        }
    };

}
