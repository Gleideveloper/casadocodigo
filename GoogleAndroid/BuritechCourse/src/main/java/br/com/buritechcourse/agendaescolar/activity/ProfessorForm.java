package br.com.buritechcourse.agendaescolar.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.buritechcourse.agendaescolar.R;
import br.com.buritechcourse.agendaescolar.model.bean.Professor;
import br.com.buritechcourse.agendaescolar.helper.ProfessorHelper;
import br.com.buritechcourse.agendaescolar.model.dao.ProfessorDAO;

import java.io.File;

public class ProfessorForm extends AppCompatActivity {

    private static final String TAG = "ProfessorForm";
    private static final String TAG_SALVAR = "SALVAR ";
    private ProfessorHelper helper;

    //Path para o arquivo de foto do professor
    private String pathArquivo;
    //Constante usada como requestCode
    private static final int FAZER_FOTO_PROFESSOR = 12345;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.professor_form_layout);
        Log.d(TAG, "Executou onCreate()");

        //Definição do título da tela
        getSupportActionBar().setTitle(R.string.title_professor_form);

        //Instancia os componentes da view
       /* EditText nomeProfessor = (EditText) findViewById(R.id.edNome);
        EditText foneProfessor = (EditText) findViewById(R.id.edFone);
        EditText siteProfessor = (EditText) findViewById(R.id.edSite);
        EditText emailProfessor = (EditText) findViewById(R.id.edEmail);
        EditText enderecoProfessor = (EditText) findViewById(R.id.edEndereco);*/
        /**
         * Instancia de componentes da view substituido pelo Padrão Helper
         */
        //Criação do objeto Helper
        helper = new ProfessorHelper(this);

        //Recuperando o professor passado como parâmetro
        final Professor professor = (Professor) getIntent().getSerializableExtra("PROFESSOR_SELECIONADO");

        if (professor != null)
            helper.setProfessor(professor);

        final Button salvarDados = (Button) findViewById(R.id.btSalvar);

        salvarDados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("onClick ", "Clicou no Botão Salvar");
                //Solicitação de serviço do Helper
                Professor professor = helper.getProfessor();

                //Criação do Objeto DAO e início da conexão com o Banco de Dados
                ProfessorDAO professorDAO = new ProfessorDAO(ProfessorForm.this);

                if (professor.getNome().isEmpty()) {
                    Toast.makeText(ProfessorForm.this, "Informe os dados do professor", Toast.LENGTH_LONG).show();
                } else {
                    if (professor.getId() == null) {
                        //Chamada do método de cadastro de professor
                        professorDAO.cadastrarProfessor(professor);
                        Toast.makeText(ProfessorForm.this, "Professor cadastrado: " + professor.getNome(), Toast.LENGTH_LONG).show();
                    } else {
                        //Atualiza os dados do professor
                        professorDAO.alterarProfessor(professor);
                        Toast.makeText(ProfessorForm.this, "Dados alterado professor: " + professor.getNome(), Toast.LENGTH_LONG).show();
                    }

                    professorDAO.close();

                    //Imprime o log com as informações do professor
                    printLogProfessor(professor);

                    finish();
                }
            }
        });

        EditText enderecoProfessor = (EditText) findViewById(R.id.edEndereco);
        enderecoProfessor.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Log.i("onKey", "Enter or Button OK pressed");
                    //Solicitação de serviço do Helper
                    salvarDados.performClick();
                }
                /*salvarDados.setVisibility(View.GONE);
                salvarDados.setEnabled(false);*/
                return false;
            }
        });

        helper.getFoto().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pathArquivo = Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";

                //Cria uma referência para o Sistema de arquivos
                File arquivo = new File(pathArquivo);

                //URI que informa onde o arquivo resultado deve ser salvo
                Uri localFoto = Uri.fromFile(arquivo);

                Intent irParaCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                irParaCamera.putExtra(MediaStore.EXTRA_OUTPUT, localFoto);
                startActivityForResult(irParaCamera, FAZER_FOTO_PROFESSOR);
            }
        });

        /**
         * Tirar duvida!!!
         * Questionamento -> porque os dados do mProfessor são impressos,
         * se antes o helper.setProfessor(new Professor()) cria uma nova instancia para Professor
         * anulando as propriedades do ProfessorHelper ?!?!
         */
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Inclusão de chave-valor no mapa
        outState.putSerializable("localArquivoFoto", pathArquivo);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //Verifica se o mapa está nulo
        if (savedInstanceState != null) {
            pathArquivo = (String) savedInstanceState.getSerializable("localArquivoFoto");
        }
        //Carrega a foto, se necessário
        if (pathArquivo != null) {
            helper.carregaFotoExternalStorage(pathArquivo);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FAZER_FOTO_PROFESSOR && resultCode == Activity.RESULT_OK) {
            helper.carregaFotoExternalStorage(pathArquivo);
        }else{
            pathArquivo = null;
        }
    }

    public void printLogProfessor(Professor professor) {
        Log.d(TAG_SALVAR, "Nome: " + professor.getNome());
        Log.d(TAG_SALVAR, "Telefone: " + professor.getTelefone());
        Log.d(TAG_SALVAR, "Site: " + professor.getSite());
        Log.d(TAG_SALVAR, "Email: " + professor.getEmail());
        Log.d(TAG_SALVAR, "Endereço: " + professor.getEndereco());
        Log.d(TAG_SALVAR, "Endereço: " + professor.getFoto());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.professor_form_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
