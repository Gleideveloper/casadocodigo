package br.com.buritechcourse.agendaescolar.adapter;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.buritechcourse.agendaescolar.R;
import br.com.buritechcourse.agendaescolar.activity.ProfessorLista;
import br.com.buritechcourse.agendaescolar.model.bean.Professor;

import java.util.List;

/**
 * Created by gleidesilva on 15/09/15.
 */
public class ProfessorListaAdapter extends BaseAdapter {
    //Lista de professores
    private final List<Professor> listaDeProfessores;
    //Activity responsável pela tela
    private final ProfessorLista activity;

    public ProfessorListaAdapter(ProfessorLista activity, List<Professor> listaDeProfessores) {
        this.listaDeProfessores = listaDeProfessores;
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return listaDeProfessores.size();
    }

    @Override
    public Object getItem(int position) {
        return listaDeProfessores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaDeProfessores.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Infla o layout na view
        View view = activity.getLayoutInflater().inflate(R.layout.item_listagem, null);
        Professor professor = listaDeProfessores.get(position);

        //Definicao de cor de fundo da linhas pares ou impares
        if (position % 2 == 0) {
            view.setBackgroundColor(activity.getResources().getColor(R.color.linha_par));
        } else {
            view.setBackgroundColor(activity.getResources().getColor(R.color.linha_impar));
        }

        int display_mode = activity.getResources().getConfiguration().orientation;
        //Configuracao dos campos
        TextView nome = (TextView) view.findViewById(R.id.item_nome);
        TextView endereco = (TextView) view.findViewById(R.id.item_endereco);
        nome.setText(professor.getNome());
        endereco.setText(professor.getEndereco());
        if (display_mode == Configuration.ORIENTATION_LANDSCAPE) {
            TextView telefone = (TextView) view.findViewById(R.id.item_telefone);
            TextView email = (TextView) view.findViewById(R.id.item_email);
            telefone.setText(professor.getTelefone());
            email.setText(professor.getEmail());
        }

        //Configuracao foto
        Bitmap bitmap;
        if (professor.getFoto() != null) {
            bitmap = BitmapFactory.decodeFile(professor.getFoto());
            if (bitmap == null) {
                bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_no_image);
            }
        } else {
            bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_no_image);
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
        ImageView foto = (ImageView) view.findViewById(R.id.item_foto);
        foto.setImageBitmap(bitmap);
        return view;
    }
}
