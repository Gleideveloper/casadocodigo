package br.com.buritechcourse.agendaescolar.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import br.com.buritechcourse.agendaescolar.R;
import br.com.buritechcourse.agendaescolar.adapter.ProfessorListaAdapter;
import br.com.buritechcourse.agendaescolar.model.bean.Professor;
import br.com.buritechcourse.agendaescolar.model.dao.ProfessorDAO;
import br.com.buritechcourse.agendaescolar.task.DisciplinaTask;
import br.com.buritechcourse.agendaescolar.task.ProfessorTask;

import java.util.List;

import static android.app.ActionBar.DISPLAY_SHOW_HOME;
import static android.app.ActionBar.DISPLAY_SHOW_TITLE;

public class ProfessorLista extends AppCompatActivity {
    private static final String TAG = ProfessorLista.class.getSimpleName();
    //Atributos da listagem
    private ListView lvProfessorList;
    //Coleção de professores
    private List<Professor> listaDeProfessores;
    //Professor selecionado no click da ListView
    private Professor professorSelecionado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Executou onCreate()");
        setContentView(R.layout.activity_professor_list);
        //Configuração para exibir o icon da aplicação
        getSupportActionBar().setIcon(R.drawable.ic_launcher);

        //Permissão para exibir o icone
        getSupportActionBar().setDisplayOptions(DISPLAY_SHOW_HOME | DISPLAY_SHOW_TITLE);
        getSupportActionBar().setTitle(R.string.title_professor_list);

        //Bind da ListView
        lvProfessorList = (ListView) findViewById(R.id.lvProfessorList);

        //Registra a ListView para Menus de Contexto
        registerForContextMenu(lvProfessorList);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.professormenucontexto, menu);
    }

    @Override
    protected void onResume() {
        //Chamada para carregar os dados
        carregarLista();
        super.onResume();
    }

    private void carregarLista() {
        //Componentes que converte Strings em Views
        //ArrayAdapter<String> adapter;
        ProfessorListaAdapter adapter;

        //Layout da listagem
        //int adapterLayout = android.R.layout.simple_list_item_1;

        //Lista auxiliar temporária para receber a lista de nomes dos professores
        //List<String> listaNomeProfessor = new ArrayList<String>();

        //Carrega a lista a partir do banco de dados
        ProfessorDAO dao =  new ProfessorDAO(this);
        listaDeProfessores = dao.listarProfessores();

       /* //Atualiza a listagem temporária de nomes
        for (Professor professor : listaDeProfessores){
            listaNomeProfessor.add(professor.getNome());
        }*/

        //Criação do adaptador
        //adapter = new ArrayAdapter<String>(this,adapterLayout,listaNomeProfessor);
        adapter = new ProfessorListaAdapter(this, listaDeProfessores);

        //Associação do adapter ao ListView
        lvProfessorList.setAdapter(adapter);

        //Configuração clicks da listagem professores
        configClickItemListProfessor();
    }

    private void excluir (){
        //Cricao do componente de confirmacao da exclusao
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        //Configuracao da mensagem
        alertBuilder.setMessage("Confirma a exclusão de: " + professorSelecionado.getNome() + " ?");
        //Configuracao do botao de confirmacao da exclusao
        alertBuilder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ProfessorDAO dao = new ProfessorDAO(ProfessorLista.this);
                //Execucao do metodo de exclusao
                dao.excluirProfessor(professorSelecionado);
                dao.close();
                //Atualizacao da listagem
                carregarLista();
                professorSelecionado = null;
            }
        });
        //configuracao da opcao de cancelamento da exclusao
        alertBuilder.setNegativeButton("Não", null);
        //Criacao da caixa de dialogo
        AlertDialog dialog = alertBuilder.create();
        dialog.setTitle("Confirmação de operação");
        //Exibicao da caixa de dialogo
        dialog.show();
    }

    private void configClickItemListProfessor() {
        //Configuração do click CURTO
        lvProfessorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ProfessorLista.this, "[Click SIMPLES] " + listaDeProfessores.get(position), Toast.LENGTH_LONG).show();
                //Pega a referencia para o professor selecionado
                professorSelecionado = listaDeProfessores.get(position);
                Intent intentForm = new Intent(ProfessorLista.this, ProfessorForm.class);
                /**
                 * Passagem de parâmetros de uma Activity para outra
                 * "PROFESSOR_SELECIONADO" -> É a Chave que deve ser recuperada em ProfessorForm
                 */
                intentForm.putExtra("PROFESSOR_SELECIONADO", professorSelecionado);
                startActivity(intentForm);
            }
        });
        //Configuração do click LONGO
        lvProfessorList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ProfessorLista.this, "[Click LONGO] " + listaDeProfessores.get(position), Toast.LENGTH_LONG).show();
                professorSelecionado = listaDeProfessores.get(position);
                return false;
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.item_menu_contexto_ligar:
            //Tipos de chamadas ACTION_DIAL e ACTION_CALL
            intent = new Intent(Intent.ACTION_CALL);
            //pega a posição da pessoa
            intent.setData(Uri.parse("tel:" + professorSelecionado.getTelefone()));
            startActivity(intent);
            break;
            case R.id.item_menu_contexto_excluir:
                //Toast.makeText(ProfessorLista.this, "Menu de Contexto - Excluir", Toast.LENGTH_LONG).show();
                excluir();
                break;
            case R.id.item_menu_contexto_enviar_sms:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("sms:" + professorSelecionado.getTelefone()));
                intent.putExtra("sms_body", "Mensagem de boas vindas :)");
                startActivity(intent);
                break;
            case R.id.item_menu_contexto_ver_no_mapa:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:2.573259,59.9813835?z=14&q=" + professorSelecionado.getEndereco()));
                startActivity(intent);
                break;
            case R.id.item_menu_contexto_navegar_site:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http:" + professorSelecionado.getSite()));
                startActivity(intent);
                break;
            case R.id.item_menu_contexto_enviar_email:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{professorSelecionado.getEmail()});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Noticia do dia");
                intent.putExtra(Intent.EXTRA_TEXT, "Governo corta Minha Casa minha vida");
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_professor_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        //Recupera o id do item do menu selecionado
        switch (item.getItemId()) {
            case R.id.menu_item_novo:
                intent = new Intent(this, ProfessorForm.class);
                startActivity(intent);
                return false;
            case R.id.menu_item_enviar:
                new ProfessorTask(this).execute();
                return false;
            case R.id.menu_item_receber:
                new DisciplinaTask(this).execute();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }
}