package br.com.designpatterns.templatemethod.exemplo2;

import java.io.IOException;
import java.util.Map;

public class GeradorPropriedadesCriptografado<T extends Object> extends
		GeradorArquivo<T> {
	private int delay;

	public GeradorPropriedadesCriptografado(int delay) {
		this.delay = delay;
	}
	@Override
	protected String gerarConteudo(Map<String, T> propriedades) {
		StringBuilder propFileBuilder = new StringBuilder();
		for (String prop : propriedades.keySet()) {
			propFileBuilder.append(prop + "=" + propriedades.get(prop) + "\n");
		}
		return propFileBuilder.toString();
	}
	@Override
	protected byte[] processar(byte[] bytes) throws IOException {
		byte[] newBytes = new byte[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			newBytes[i] = (byte) ((bytes[i] + delay) % Byte.MAX_VALUE);
		}
		return newBytes;
	}

}
