package br.com.designpatterns.templatemethod.exemplo1;

public class Cliente {

	public static void main(String[] args) {
		ClasseAbstrata c = new ClasseConcreta();
		c.metodoTemplate();
	}

}
