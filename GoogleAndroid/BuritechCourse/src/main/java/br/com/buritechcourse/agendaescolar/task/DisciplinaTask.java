package br.com.buritechcourse.agendaescolar.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import br.com.buritechcourse.agendaescolar.converter.DisciplinaConverter;
import br.com.buritechcourse.agendaescolar.integracao.WebClient;
import br.com.buritechcourse.agendaescolar.model.bean.Conteudo;
import br.com.buritechcourse.agendaescolar.model.bean.Disciplina;
import br.com.buritechcourse.agendaescolar.model.dao.ConteudoDAO;
import br.com.buritechcourse.agendaescolar.model.dao.DisciplinaDAO;

import java.util.List;

/**
 * Created by gleidesilva on 22/09/15.
 */
public class DisciplinaTask extends AsyncTask<Object, Object, String>{
    //private final String url = "http://192.168.1.129:8080/AlunoWeb/receber-json";
    private final String url = "http://IP-DO-SERVIDOR:8080/AlunoWeb/receber-json";
    private static final String TAG_CLASS_NAME = DisciplinaTask.class.getSimpleName();
    private Context context;
    private ProgressDialog progressDialogBar;

    public DisciplinaTask(Context context) {
        this.context = context;
    }

    /**
     * Método de callBack invocado ANTES da execução da tarefa
     */
    @Override
    protected void onPreExecute() {
        progressDialogBar = ProgressDialog.show(context, "Aguarde...", "RECEBENDO lista de disciplinas", true, true);
    }

    /**
     * Método de callBack invocado APÓS a conclusão da tarefa
     * @param result
     */
    @Override
    protected void onPostExecute(String result) {
        progressDialogBar.dismiss();
        Toast.makeText(context, "Processamento concluído " + result, Toast.LENGTH_LONG).show();
    }

    /**
     * Método de execução da tarefa - Envio de dados de disciplinas
     * @param params
     * @return String com a resposta do servidor
     */
    @Override
    protected String doInBackground(Object... params) {
        String jsonResposta = new WebClient(url).post("{\"metodo\":\"listarDisciplinas\"}");
        Log.i(TAG_CLASS_NAME, "JSON RECEBIDO: " + jsonResposta);
        List<Disciplina> lista = new DisciplinaConverter().fromJSON(jsonResposta);
        Log.i(TAG_CLASS_NAME, "LISTA GERADA: " + lista);

        DisciplinaDAO disciplinaDAO = new DisciplinaDAO(context);
        ConteudoDAO conteudoDAO = new ConteudoDAO(context);
        conteudoDAO.deletarConteudo();
        disciplinaDAO.deletarDisciplina();

        for (Disciplina disciplina : lista){
            disciplinaDAO.cadastrarDisciplina(disciplina);
            for (Conteudo conteudo : disciplina.getPlanoDeAula()){
                conteudoDAO.cadastrarConteudo(conteudo);
            }
        }

        disciplinaDAO.close();
        conteudoDAO.close();
        return jsonResposta;
    }
}
