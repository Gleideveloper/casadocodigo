package br.com.designpatterns.strategy;

public class CalculoDiaria implements CalculoValor {
	private double valorDiaria;
	private int periodo;
	
	public CalculoDiaria(double valorDiaria) {
		this.valorDiaria = valorDiaria;
	}

	public double calcular() {
		return valorDiaria * Math.ceil(periodo/HORA);
	}

	@Override
	public double calcular(long periodo, Veiculo veiculo) {
		return 0;
	}

}
