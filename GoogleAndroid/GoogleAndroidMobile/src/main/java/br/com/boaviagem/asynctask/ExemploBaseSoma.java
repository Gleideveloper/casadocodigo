package br.com.boaviagem.asynctask;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import br.com.boaviagem.R;

/**
 * Created by gleides on 26/05/15.
 */
public abstract class ExemploBaseSoma extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_soma);

        Button button = (Button) findViewById(R.id.btnOK);
        button.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText campo1 = (EditText) findViewById(R.id.editText1);
                EditText campo2 = (EditText) findViewById(R.id.editText2);
                int vlr1 = Integer.parseInt(campo1.getText().toString());
                int vlr2 = Integer.parseInt(campo2.getText().toString());
                somar(vlr1, vlr2);
            }
        });
    }

    protected abstract void somar(int vlr1, int vlr2);
}
