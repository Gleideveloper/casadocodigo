package br.com.buritechcourse.agendaescolar.converter;

import br.com.buritechcourse.agendaescolar.model.bean.Conteudo;
import br.com.buritechcourse.agendaescolar.model.bean.Disciplina;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gleidesilva on 22/09/15.
 */
public class DisciplinaConverter {
    public List<Disciplina> fromJSON (String jsonRecebido) {
    List<Disciplina> lista = new ArrayList<Disciplina>();
        try {
            JSONObject jsonObject = new JSONObject(jsonRecebido);
            JSONArray listarDisciplinas = jsonObject.getJSONArray("listaDeDisciplinas");
            for (int i = 0; i < listarDisciplinas.length(); i++) {
                JSONObject disciplinaJSON =  listarDisciplinas.getJSONObject(i);

                Disciplina disciplina = new Disciplina();
                disciplina.setId(disciplinaJSON.getLong("id"));
                disciplina.setNome(disciplinaJSON.getString("nome"));
                disciplina.setEmenta(disciplinaJSON.getString("ementa"));
                disciplina.setCargaHoraria(disciplinaJSON.getLong("cargahoraria"));

                JSONArray planoDeAula =  disciplinaJSON.getJSONArray("planoDeAula");
                for (int j = 0; j < planoDeAula.length(); j++) {
                    JSONObject planoJSON =  planoDeAula.getJSONObject(j);
                    Conteudo conteudo = new ConteudoConverter().fromJSON(planoJSON);
                    conteudo.setDisciplina(disciplina);
                    disciplina.getPlanoDeAula().add(conteudo);
                }
                lista.add(disciplina);
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
        return lista;
    }
}
