package br.com.buritechcourse.agendaescolar.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;
import br.com.buritechcourse.agendaescolar.R;

/**
 * Created by gleidesilva on 14/09/15.
 */
public class SMSReceiver extends BroadcastReceiver {
    /*@Override
    public void onReceive(Context context, Intent intent) {
        *//**
         * Transforma o SMS em um array de bytes
         *//*
        //Recuperar dados do SMS recebido
        Bundle bundle = intent.getExtras();
        Object[] messages = (Object[]) bundle.get("pdus");
        byte[] message = (byte[]) messages[0];
        *//**
         * Cria um SMSMessage a partir do array de bytes
        *//*
        //Objeto para acesso a dados do SMS
        SmsMessage smsMessage = SmsMessage.createFromPdu(message);
        String telefone = smsMessage.getDisplayOriginatingAddress();

        //Tocar uma MP3 salva em resource
        MediaPlayer mediaPlayer =  MediaPlayer.create(context, R.raw.get_lucky);
        mediaPlayer.start();
        Toast.makeText(context, "SMS recebido do fone: " + telefone, Toast.LENGTH_LONG).show();
    }*/

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String TAG = "SMSBroadcastReceiver";

    /**
     * Exemplo de recebimento de msg -> http://stackoverflow.com/questions/4117701/android-sms-broadcast-receiver
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Intent recieved: " + intent.getAction());

        if (intent.getAction() == SMS_RECEIVED) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[])bundle.get("pdus");
                final SmsMessage[] smsMessages = new SmsMessage[pdus.length];
                byte[] message = (byte[]) pdus[0];
                SmsMessage smsMessage = SmsMessage.createFromPdu(message);

                for (int i = 0; i < pdus.length; i++) {
                    smsMessages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                }
                if (smsMessages.length > -1) {
                    String telefone = smsMessage.getDisplayOriginatingAddress();

                    //Tocar uma MP3 salva em resource
                    /*MediaPlayer mediaPlayer =  MediaPlayer.create(context, R.raw.get_lucky);
                    mediaPlayer.start();*/
                    Toast.makeText(context, "SMS recebido do fone: " + telefone, Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Message recieved: " + smsMessages[0].getMessageBody());
                }
            }
        }
    }
}
