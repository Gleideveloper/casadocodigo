package br.com.buritechcourse.agendaescolar.converter;

import android.util.Log;
import br.com.buritechcourse.agendaescolar.model.bean.Professor;
import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

/**
 * Created by gleidesilva on 18/09/15.
 */
public class ProfessorConverter {
    public String toJSON (List<Professor> listaProfessor) {
        try {
            JSONStringer jsonStringer = new JSONStringer();
            jsonStringer.object().key("listaAtualizada").array();

            for (Professor professor :  listaProfessor){
                jsonStringer.object()
                        .key("id").value(professor.getId())
                        .key("nome").value(professor.getNome())
                        .key("telefone").value(professor.getTelefone())
                        .key("endereco").value(professor.getEndereco())
                        .key("site").value(professor.getSite())
                        .key("email").value(professor.getEmail())
                        .endObject();
            }
            jsonStringer.endArray().endObject();
            return jsonStringer.toString();
        } catch (JSONException e) {
            Log.i("PROFESSOR-CONVERTER", e.getMessage());
            return null;
        }
    }
}
