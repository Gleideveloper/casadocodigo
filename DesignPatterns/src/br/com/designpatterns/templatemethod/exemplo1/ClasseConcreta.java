package br.com.designpatterns.templatemethod.exemplo1;

public class ClasseConcreta extends ClasseAbstrata {

	@Override
	protected void passoAlgoritmoA() {
		System.out.println("ClasseConcreta passoAlgoritmoA");
	}

	@Override
	protected void passoAlgoritmoB() {
		System.out.println("ClasseConcreta passoAlgoritmoB");
	}

}
