package br.com.designpatterns.nullobject;

public class HTTPServletRequest {

	private String str;
	private Object obj;

	public void setAttribute(String string, Object valor) {
		this.str = string;
		this.obj = valor;
		
	}

	public Object getAttribute(String string) {
		return this.str;
	}

}
