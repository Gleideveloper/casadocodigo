package br.com.buritechcourse.agendaescolar.model.bean;

import java.io.Serializable;

/**
 * Created by gleidesilva on 21/09/15.
 */
public class Conteudo implements Serializable {
    private Long id;
    private String data;
    private String descricao;
    private String sala;
    private String horario;
    private Disciplina disciplina;

    @Override
    public String toString() {
        return data + " - " + descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }
}
