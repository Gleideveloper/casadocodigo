package br.com.boaviagem;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViagemListActivity extends ListActivity implements OnItemClickListener {
    private List<Map<String, Object>> viagens;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, Object> map =  viagens.get(position);
        String destino = (String) map.get("destino");
        String mensagem = "Viagem selecionada: " + destino;
        Toast.makeText(this, mensagem, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, GastoListActivity.class));
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String [] keyMap = {"imagem","destino","data","total"};
		int [] valueMap = {R.id.tipoViagem, R.id.destino, R.id.data, R.id.valor};

		SimpleAdapter adapter = new SimpleAdapter(this,listarViagens(), R.layout.lista_viagem , keyMap, valueMap);
		setListAdapter(adapter);

        getListView().setOnItemClickListener(this);
	}

	private List<Map<String, Object>> listarViagens() {
		viagens = new ArrayList<Map<String,Object>>();

		Map<String, Object> item = new HashMap<String, Object>();
		item.put("imagem", R.drawable.negocio);
		item.put("destino", "São Paulo");
		item.put("data", "02/12/2012 a 03/12/2012");
		item.put("total", "Gasto Total: R$ 314,98");
		viagens.add(item);

	    item = new HashMap<String, Object>();
		item.put("imagem", R.drawable.lazer);
		item.put("destino", "Maceió");
		item.put("data", "14/05/2014 a 03/06/2014");
		item.put("total", "Gasto Total: R$ 2.580,95");
		viagens.add(item);

		return viagens;
	}

}
