package br.com.designpatterns.templatemethod.exemplo2;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

public abstract class GeradorArquivo<T extends Object> {

	public final void gerarArquivo(String nome, Map<String, T> propriedades) throws IOException {
		String conteudo = gerarConteudo(propriedades);
		byte[] bytes = conteudo.getBytes();
		bytes = processar(bytes);
		FileOutputStream fileout = new FileOutputStream(nome);
		fileout.write(bytes);
		fileout.close();
	}

	protected abstract String gerarConteudo(Map<String, T> propriedades);

	protected byte[] processar(byte[] bytes) throws IOException {
		return bytes;
	}


}
