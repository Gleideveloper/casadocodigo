package br.com.buritechcourse.agendaescolar.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import br.com.buritechcourse.agendaescolar.model.bean.Conteudo;
import br.com.buritechcourse.agendaescolar.model.bean.Disciplina;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gleidesilva on 22/09/15.
 */
public class ConteudoDAO extends DataBaseHelper {
    private static final String TAG_CLASS_NAME = ConteudoDAO.class.getSimpleName();
    private static final int ID_CONTEUDO = 0;
    private static final int DESCRICAO = 1;
    private static final int SALA = 2;
    private static final int HORARIO= 3;
    private static final int DATA = 4;

    public ConteudoDAO(Context context) {
        super(context);
        Log.i(TAG_CLASS_NAME, " Método construtor");
    }

    public void cadastrarConteudo(Conteudo conteudo) {
        ContentValues values = new ContentValues();
        values.put("descricao", conteudo.getDescricao());
        values.put("sala", conteudo.getSala());
        values.put("horario", conteudo.getHorario());
        values.put("data", conteudo.getData());
        values.put("idDisciplina", conteudo.getDisciplina().getId());

        getWritableDatabase().insert(TABELA_CONTEUDO, null, values);
        Log.i(TAG_CLASS_NAME, " Conteúdo cadastrado: " + conteudo);
    }

    public void deletarConteudo() {
        String dml = "Delete from " + TABELA_CONTEUDO;
        getWritableDatabase().execSQL(dml);
    }

    public List<Conteudo> listarConteudo(Disciplina disciplina) {
        List<Conteudo> lista = new ArrayList<Conteudo>();
        String sql = "Select * from " + TABELA_CONTEUDO +
                     "Where idDisciplina = ? order by data";
        Cursor cursor = getReadableDatabase().rawQuery(sql, new String[]{disciplina.getId().toString()});
        try {
            while (cursor.moveToNext()){
                Conteudo conteudo = new Conteudo();
                conteudo.setId(cursor.getLong(ID_CONTEUDO));
                conteudo.setDescricao(cursor.getString(DESCRICAO));
                conteudo.setSala(cursor.getString(SALA));
                conteudo.setHorario(cursor.getString(HORARIO));
                conteudo.setData(cursor.getString(DATA));
                conteudo.setDisciplina(disciplina);
                lista.add(conteudo);
            }
        }catch (SQLException e ){
            Log.e(TAG_CLASS_NAME, e.getMessage());
        } finally {
            cursor.close();
        }
        return lista;
    }
}