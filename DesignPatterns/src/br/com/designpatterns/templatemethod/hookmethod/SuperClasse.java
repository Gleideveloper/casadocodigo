package br.com.designpatterns.templatemethod.hookmethod;

public abstract class SuperClasse {
	
	public void metodoPrincipal(){
		//executa lógica comum
		metodoGancho();
		//executa lógica comum
	}

	protected abstract void metodoGancho();

}
