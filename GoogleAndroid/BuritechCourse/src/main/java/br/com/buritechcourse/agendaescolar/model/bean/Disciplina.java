package br.com.buritechcourse.agendaescolar.model.bean;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gleidesilva on 21/09/15.
 */
public class Disciplina implements Serializable {
    private Long id;
    private String nome;
    private String ementa;
    private Long cargaHoraria;
    private List<Conteudo> planoDeAula = new ArrayList<Conteudo>();

    @Override
    public String toString() {
        String retorno = this.nome;
        if (planoDeAula != null)
            retorno += planoDeAula;
        return retorno;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public Long getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(Long cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public List<Conteudo> getPlanoDeAula() {
        return planoDeAula;
    }

    public void setPlanoDeAula(List<Conteudo> conteudos) {
        this.planoDeAula = conteudos;
    }
}
