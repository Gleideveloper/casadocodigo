package br.com.buritechcourse.agendaescolar.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import br.com.buritechcourse.agendaescolar.R;
import br.com.buritechcourse.agendaescolar.model.bean.Disciplina;
import br.com.buritechcourse.agendaescolar.model.dao.DisciplinaDAO;

/**
 * Created by gleidesilva on 22/09/15.
 */
public class DisciplinaMasterFragment extends Fragment {
    //Referencia para o layout inflado
    private View layoutMaster;

    //Metodo para carregar lista de Disciplinas em uma ListView
    private void carregarLista() {
        //Instancia de um layout simples nativo do android
        int layoutAdapter = android.R.layout.simple_expandable_list_item_1;

        //Instancia de um ArrayAdapter para receber a lista de Disciplinas
        final ArrayAdapter<Disciplina> adapter;

        //Cria a instancia DAO referenciando o contexto do layoutMaster
        DisciplinaDAO disciplinaDAO = new DisciplinaDAO(layoutMaster.getContext());

        //Cria a instancia de uma ListView referenciando a ListView disciplinas dentro do layout disciplina_master_fragment
        ListView lvDisciplinas = (ListView) layoutMaster.findViewById(R.id.lvDisciplinas);

        //Referencia o adapter com o ArrayAdapter recebendo como parametros: Activity, layoutAdapter, listaDeDisciplinas
        adapter = new ArrayAdapter<Disciplina>(getActivity(),layoutAdapter,disciplinaDAO.listarDisciplina());

        //Registra e seta o Adapter na ListView de Disciplinas
        lvDisciplinas.setAdapter(adapter);

        //Fecha a conexão com o dataBase
        disciplinaDAO.close();

        //Evento de click do item da ListView
        lvDisciplinas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Disciplina selecionada = adapter.getItem(position);
                Toast.makeText(getActivity(), "Disciplina: " + selecionada, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.layoutMaster = inflater.inflate(R.layout.disciplina_master_fragment, container, false);
        carregarLista();
        return layoutMaster;
    }
}
