package br.com.buritechcourse.agendaescolar.model.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import br.com.buritechcourse.agendaescolar.model.bean.Conteudo;

/**
 * Created by gleidesilva on 21/09/15.
 */
public abstract class DataBaseHelper extends SQLiteOpenHelper{
    //Constantes para controle de versão e padronização
    public static final String TAG_CLASS_NAME = DataBaseHelper.class.getSimpleName();
    public static final int VERSAO = 1;
    public static final String BANCO_DE_DADOS = "AgendaEscolar";
    public static final String TABELA_PROFESSOR = "PROFESSOR";
    public static final String TABELA_DISCIPLINA = "DISCIPLINA";
    public static final String TABELA_CONTEUDO = "CONTEUDO";

    private Context context;

    public DataBaseHelper(Context context) {
        super(context, BANCO_DE_DADOS, null, VERSAO);
        this.context = context;
        Log.i(TAG_CLASS_NAME, "Método construtor");
    }

    public Context getContext() {
        return context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG_CLASS_NAME, "Criação do banco de dados" + BANCO_DE_DADOS);
        String ddl = "CREATE TABLE " + TABELA_PROFESSOR + " ( " +
                " id INTEGER PRIMARY KEY, " +
                " nome TEXT, telefone TEXT,  endereco TEXT, " +
                " site TEXT,  email TEXT, foto TEXT)";
        db.execSQL(ddl);
        Log.d(TAG_CLASS_NAME, "Criação da tabela " + TABELA_PROFESSOR);

        ddl = "CREATE TABLE " + TABELA_DISCIPLINA + " ( " +
                " id INTEGER PRIMARY KEY, " +
                " nome TEXT, ementa TEXT,  cargahoraria NUMERIC)";
        db.execSQL(ddl);
        Log.d(TAG_CLASS_NAME, "Criação da tabela " + TABELA_DISCIPLINA);

        ddl = "CREATE TABLE " + TABELA_CONTEUDO + " ( " +
                " id INTEGER PRIMARY KEY, " +
                " descricao TEXT, sala TEXT,  horario TEXT, " +
                " data DATE,  idDisciplina INTEGER, " +
                "FOREIGN KEY (idDisciplina) REFERENCES Disciplina(id))";
        db.execSQL(ddl);
        Log.d(TAG_CLASS_NAME, "Criação da tabela " + TABELA_CONTEUDO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String ddl = "DROP TABLE IF EXISTS " + TABELA_PROFESSOR;
        db.execSQL(ddl);
        ddl = "DROP TABLE IF EXISTS " + TABELA_CONTEUDO;
        db.execSQL(ddl);
        ddl = "DROP TABLE IF EXISTS " + TABELA_DISCIPLINA;
        db.execSQL(ddl);

        onCreate(db);
        Log.i(TAG_CLASS_NAME, "Atualização do banco de dados: " + BANCO_DE_DADOS);
    }
}
