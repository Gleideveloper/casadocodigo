package br.com.designpatterns.templatemethod.exemplo2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) throws IOException {
		Map<String, String> props = new HashMap<String, String>();
		props.put("K1", new String("V1"));
		props.put("K2", new String("V2"));
		props.put("K3", new String("V3"));
		props.put("K4", new String("V4"));
		props.put("K5", new String("V5"));
		props.put("K6", new String("V6"));
		props.put("K7", new String("V7"));
		/**
		 * Métodos que chamam criam as subclasse
		 * que implementam a SuperClasse GeradorArquivo
		 */
		geraPropriedadeCriptografado(props);
		geraXmlZip(props);
	}

	public static void geraPropriedadeCriptografado(Map<String, String> props) throws IOException {
		GeradorArquivo<String> geradorProCript = new GeradorPropriedadesCriptografado<>(5);
		geradorProCript.gerarArquivo("Exemplo1", props);
	}

	private static void geraXmlZip(Map<String, String> props) throws IOException {
		GeradorArquivo<String> geradorXmlZip = new GeradorXMLCompactado<String>();
		geradorXmlZip.gerarArquivo("Exemplo2", props);
	}

}
