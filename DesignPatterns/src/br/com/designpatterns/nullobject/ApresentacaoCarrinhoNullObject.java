package br.com.designpatterns.nullobject;

public class ApresentacaoCarrinhoNullObject {
	
	public void colocarInformacoesCarrinho(HTTPServletRequest request) {
		/**
		 * Padrão Null Object: Na listagem a seguir, é possível observar como o código fica mais simples
		 * com a eliminação da parte responsável pelo tratamento dos valores nulos. Outra coisa que precisaria
		 * ser alterada é a própria classe CookieFactory que deve retornar uma instância de CarrinhoNulo
		 * em vez de null quando nenhum carrinho do usuário for encontrado.
		 */
		
		Carrinho c = CookieFactory.criarCarrinho(request);
			request.setAttribute("valor", c.getValor());
			request.setAttribute("qtd", c.getTamanho());
			
		if (request.getAttribute("username") == null) {
				request.setAttribute("userCarrinho", c.getNomeUsuario());
		} else {
			request.setAttribute("userCarrinho",
					request.getAttribute("username"));
		}
	}

}
