package br.com.designpatterns.strategy;

public interface CalculoValor {
	static final int HORA = 0;
	static final long DIA = 0;
	static final long MES = 0;
	
	public double calcular();

	public double calcular(long periodo, Veiculo veiculo);
}
