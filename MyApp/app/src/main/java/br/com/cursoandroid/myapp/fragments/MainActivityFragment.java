package br.com.cursoandroid.myapp.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import br.com.cursoandroid.myapp.activities.MainActivity;
import br.com.cursoandroid.myapp.R;

/**
 * Created by gleides.s on 22/12/15.
 */
public class MainActivityFragment extends Fragment {
    private static final String TAG = MainActivity.class.getSimpleName();

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container,false);

        btn1 = (Button) view.findViewById(R.id.button01);
        btn1.setOnClickListener(onClickBtn01);
        btn2 = (Button) view.findViewById(R.id.button02);
        btn3 = (Button) view.findViewById(R.id.button03);
        btn4 = (Button) view.findViewById(R.id.button04);
        btn5 = (Button) view.findViewById(R.id.button05);
        btn6 = (Button) view.findViewById(R.id.button06);
        btn7 = (Button) view.findViewById(R.id.button07);
        btn8 = (Button) view.findViewById(R.id.button08);

        return view;
    }

    View.OnClickListener onClickBtn01 =  new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment musicPlayerSampleFragment = new MusicPlayerSampleFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(getId(), musicPlayerSampleFragment, "fragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    };

}
