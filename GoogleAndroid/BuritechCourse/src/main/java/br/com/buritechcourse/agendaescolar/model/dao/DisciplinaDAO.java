package br.com.buritechcourse.agendaescolar.model.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import br.com.buritechcourse.agendaescolar.model.bean.Disciplina;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gleidesilva on 22/09/15.
 */
public class DisciplinaDAO extends DataBaseHelper {
    private static final String TAG_CLASS_NAME = DisciplinaDAO.class.getSimpleName();
    private static final int ID_DISCIPLINA = 0;
    private static final int NOME_DISCIPLINA = 1;
    private static final int EMENTA_DISCIPLINA = 2;
    private static final int CARGA_HORARIA_DISCIPLINA = 3;

    public DisciplinaDAO(Context context) {
        super(context);
        Log.i(TAG_CLASS_NAME, " Método construtor");
    }

    public void cadastrarDisciplina (Disciplina disciplina) {
        ContentValues values = new ContentValues();
        values.put("nome", disciplina.getNome());
        values.put("ementa", disciplina.getEmenta());
        values.put("cargahoraria", disciplina.getCargaHoraria());

        getWritableDatabase().insert(TABELA_DISCIPLINA, null, values);
        Log.i(TAG_CLASS_NAME, " Disciplina cadastrada: " + disciplina);
    }
    public void deletarDisciplina () {
        String dml = "Delete from " + TABELA_DISCIPLINA;
        getWritableDatabase().execSQL(dml);
    }

    public List<Disciplina> listarDisciplina (){
        List<Disciplina> lista =  new ArrayList<Disciplina>();
        String sql = "Select * from " + TABELA_DISCIPLINA + " order by nome";
        Cursor cursor = getReadableDatabase().rawQuery(sql,null);

        try{
            while (cursor.moveToNext()){
                Disciplina disciplina = new Disciplina();
                disciplina.setId(cursor.getLong(ID_DISCIPLINA));
                disciplina.setNome(cursor.getString(NOME_DISCIPLINA));
                disciplina.setEmenta(cursor.getString(EMENTA_DISCIPLINA));
                disciplina.setCargaHoraria(cursor.getLong(CARGA_HORARIA_DISCIPLINA));

                ConteudoDAO conteudoDAO = new ConteudoDAO(getContext());
                disciplina.setPlanoDeAula(conteudoDAO.listarConteudo(disciplina));
                lista.add(disciplina);
            }
        }catch (SQLException e){
            Log.e(TAG_CLASS_NAME, e.getMessage());
        } finally {
            cursor.close();
        }
        return lista;
    }
}
