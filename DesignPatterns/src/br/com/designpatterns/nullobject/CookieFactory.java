package br.com.designpatterns.nullobject;

public class CookieFactory {

	public static Carrinho criarCarrinho(HTTPServletRequest request) {
		return (request == null ? new CarrinhoNulo() : new Carrinho());
	}

}
